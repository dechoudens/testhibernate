package ch.lysoft.SqlLiteHibernate.dao;

import ch.lysoft.SqlLiteHibernate.manager.HibernateManager;
import ch.lysoft.SqlLiteHibernate.model.Croissant;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class CroissantDaoImpl extends Dao implements CroissantDao{

    public CroissantDaoImpl(HibernateManager manager) {
        super(manager);
    }

    @Override
    public void addCroissant(Croissant croissant) {
        EntityManager entityManager = manager.getEntityManager();
        EntityTransaction et = null;

        try{
            et = entityManager.getTransaction();
            et.begin();
            entityManager.persist(croissant);
            et.commit();
        }
        catch (Throwable e) {
            if(et == null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public Croissant getCroissant(Long id) {
        EntityManager entityManager = manager.getEntityManager();
        String query = "SELECT c FROM Croissant c WHERE c.id = :croissantId";

        TypedQuery<Croissant> tq = entityManager.createQuery(query, Croissant.class);
        tq.setParameter("croissantId", id);

        Croissant croissant = null;
        try{
            croissant = tq.getSingleResult();
            if(croissant != null){
                System.out.println("Croissant " + id + " retrieved");
            }
            else{
                System.err.println("Croissant " + id + " doesn't exist");
            }
        }
        catch(Throwable e){
            e.printStackTrace();
        }
        finally {
            entityManager.close();
        }

        return croissant;
    }

    @Override
    public void updateCroissant(Long id, Integer price, String description, Date datePeremption, Boolean isDelicious) {
        EntityManager entityManager = manager.getEntityManager();
        EntityTransaction et = null;

        try{
            et = entityManager.getTransaction();
            et.begin();

            Croissant croissant = entityManager.find(Croissant.class, id);
            if(croissant != null){
                croissant.setDelicious(isDelicious);
                croissant.setDescription(description);
                croissant.setDatePeremption(datePeremption);
                croissant.setPrice(price);

                entityManager.persist(croissant);
                et.commit();

                System.out.println("Croissant " + id + " updated");
            }
            else{
                System.err.println("Croissant " + id + " doesn't exist");
            }
        }
        catch (Throwable e) {
            if(et == null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public void deleteCroissant(Long id) {

        EntityManager entityManager = manager.getEntityManager();
        EntityTransaction et = null;

        try{
            et = entityManager.getTransaction();
            et.begin();

            Croissant croissant = entityManager.find(Croissant.class, id);
            if(croissant != null){
                entityManager.remove(croissant);
                et.commit();

                System.out.println("Croissant " + id + " deleted");
            }
            else{
                System.err.println("Croissant " + id + " doesn't exist");
            }
        }
        catch (Throwable e) {
            if(et == null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public List<Croissant> getCroissants() {
        EntityManager entityManager = manager.getEntityManager();
        String query = "SELECT c FROM Croissant c WHERE c.id IS NOT NULL";

        TypedQuery<Croissant> tq = entityManager.createQuery(query, Croissant.class);

        List<Croissant> croissants = null;
        try{
            croissants  = tq.getResultList();

            System.out.println(croissants);
        }
        catch(Throwable e){
            e.printStackTrace();
        }
        finally {
            entityManager.close();
        }

        return croissants;
    }


}
