package ch.lysoft.SqlLiteHibernate.dao;

import ch.lysoft.SqlLiteHibernate.manager.HibernateManager;

public abstract class Dao {
    protected HibernateManager manager;

    public Dao(HibernateManager manager){
        this.manager = manager;
    }
}
