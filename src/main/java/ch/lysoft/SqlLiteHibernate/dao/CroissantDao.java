package ch.lysoft.SqlLiteHibernate.dao;

import ch.lysoft.SqlLiteHibernate.model.Croissant;

import java.util.Date;
import java.util.List;

public interface CroissantDao {

    void addCroissant(Croissant croissant);

    Croissant getCroissant(Long id);

    void updateCroissant(Long id, Integer price, String description, Date datePeremption, Boolean isDelicious);

    void deleteCroissant(Long id);

    List<Croissant> getCroissants();
}
