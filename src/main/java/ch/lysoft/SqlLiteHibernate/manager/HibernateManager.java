package ch.lysoft.SqlLiteHibernate.manager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateManager {
    private static HibernateManager instance;
    private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("SqlLiteHibernate");

    public static HibernateManager getInstance(){
        if(instance == null){
            instance = new HibernateManager();
        }

        return instance;
    }

    public EntityManager getEntityManager(){
        return ENTITY_MANAGER_FACTORY.createEntityManager();
    }

    public static void close(){
        ENTITY_MANAGER_FACTORY.close();
    }
}
