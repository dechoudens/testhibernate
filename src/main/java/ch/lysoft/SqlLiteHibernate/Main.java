package ch.lysoft.SqlLiteHibernate;

import ch.lysoft.SqlLiteHibernate.manager.HibernateManager;

public class Main {

    public static void main(String[] args) {
        HibernateManager hManager = HibernateManager.getInstance();
        try {
            new App(hManager).execute();
        }
        catch (Throwable e){
            e.printStackTrace();
        }
        finally {
            HibernateManager.close();
        }
    }
}
