package ch.lysoft.SqlLiteHibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "croissant")
public class Croissant implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name="price", nullable = false)
    private Integer price;
    @Column(name="description", nullable = false)
    private String description;
    @Column(name="datePeremption", nullable = false)
    private Date datePeremption;
    @Column(name="isDelicious", nullable = false)
    private Boolean isDelicious;

    public Croissant() {
    }

    public Croissant(Long id, Integer price, String description, Date datePeremption, Boolean isDelicious) {
        this.id = id;
        this.price = price;
        this.description = description;
        this.datePeremption = datePeremption;
        this.isDelicious = isDelicious;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(Date datePeremption) {
        this.datePeremption = datePeremption;
    }

    public Boolean getDelicious() {
        return isDelicious;
    }

    public void setDelicious(Boolean delicious) {
        isDelicious = delicious;
    }

    @Override
    public String toString() {
        return "Croissant{" +
                "id=" + id +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", datePeremption=" + datePeremption +
                ", isDelicious=" + isDelicious +
                '}';
    }
}
