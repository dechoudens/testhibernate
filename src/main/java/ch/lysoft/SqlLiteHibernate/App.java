package ch.lysoft.SqlLiteHibernate;

import ch.lysoft.SqlLiteHibernate.dao.CroissantDao;
import ch.lysoft.SqlLiteHibernate.dao.CroissantDaoImpl;
import ch.lysoft.SqlLiteHibernate.manager.HibernateManager;
import ch.lysoft.SqlLiteHibernate.model.Croissant;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class App {
    private final CroissantDao croissantDao;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    public App(HibernateManager manager) {
        croissantDao = new CroissantDaoImpl(manager);
    }

    public void execute() throws ParseException {
        croissantDao.addCroissant(new Croissant(1L, 200, "Croissant au Beur", sdf.parse("09.11.2022"), true));
        croissantDao.addCroissant(new Croissant(2L, 500, "Croissant au Chocolat", sdf.parse("05.06.2022"), true));
        croissantDao.addCroissant(new Croissant(3L, 100, "Croissant à la vanille", sdf.parse("15.04.2022"), true));
        croissantDao.addCroissant(new Croissant(4L, 20, "Croissant Naturel", sdf.parse("02.10.2022"), true));
        croissantDao.addCroissant(new Croissant(5L, 78, "Croissant réservé à Yvan", sdf.parse("20.09.2022"), true));

        croissantDao.getCroissants();
        Croissant croissant = croissantDao.getCroissant(4L);
        System.out.println(croissant);
        croissantDao.updateCroissant(4L, 520, "Croissant Nature", croissant.getDatePeremption(), true);
        croissant = croissantDao.getCroissant(4L);
        System.out.println(croissant);

        croissantDao.deleteCroissant(2L);
        croissantDao.getCroissants();
    }


}
